<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Panda' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ih{QJB8JQSJN[][PuH`2g %!XpevZ:WAPx#5W7eVFmkuTX,.h+!OLMLS%JisF%X1' );
define( 'SECURE_AUTH_KEY',  '<t|WO|; vLSO6@G&.W?c?|?2Z(s+xXU%je f_3uDH`~OO#b`e3om>&<YU `&X)sh' );
define( 'LOGGED_IN_KEY',    ']?UZ2;]dhNRHQ-30C9QPt|;7*Slw(M0-:Z`WysR8<%u3]q@&|GrHG/?[mDe}My V' );
define( 'NONCE_KEY',        '2?M/aFuZ[TQ~UIpDqbx*:<jiVT5MOw60:k8v;.N~gJhJ)a>mO~%0EDaHWW~9Mj$W' );
define( 'AUTH_SALT',        'INC0zMGk{8%qORo$t1}b=EwYYqd|d|URLKs]Qrth<@Xu;0z,7V;YU&j?HwA@r5SH' );
define( 'SECURE_AUTH_SALT', '8u34m,^X.m$D>%y,5t$R0Pp|$jMN(d`?)|gc#gS^^@nje3`!;VJ60TX($E1KQ=U]' );
define( 'LOGGED_IN_SALT',   '=%tKXBlACI]8wOkD6][<1G=; ]GTnI]6TDUnT>L3Hyvb(89YsHRU=SJI_LJdYgwz' );
define( 'NONCE_SALT',       '%Q21gjobn42hbYEk7nCeY7<Wy+:xIR8,OU9_aSlN*c8(pDr-BP7G>]WhNt(m!FI=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
